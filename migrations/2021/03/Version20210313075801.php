<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210313075801 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'nationality.overview', 'hash' => '5ed6e1b85c43d051ec07c68b9b5df338', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Národnosti', 'plural1' => '', 'plural2' => ''],
            ['original' => 'nationality.overview.title', 'hash' => '290dcf10753cb3fd1525b11a017cb3ec', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Národnosti|přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.nationality.overview.is-preferred', 'hash' => 'daa7e0cbe36aa1893e26471f8974f557', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Preferovaný', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.nationality.overview.name', 'hash' => '130dfeb07a224a71a78e926babf7ec5c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.nationality.overview.short-name', 'hash' => '39b92b1ccf7663035681ee3c790ae7c2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zkratka', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.nationality.overview.alpha-code2', 'hash' => 'ad1bc3afd730b29b6baf15adab6a8db2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Alfa 2', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.nationality.overview.alpha-code3', 'hash' => '029b86414d5339f658c29b8ee195373b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Alfa 3', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.nationality.title', 'hash' => '1538ea7518c0684dbdb7367574a31407', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Národnosti', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.nationality.description', 'hash' => 'db8cfade14522bb8d9da8a260f2454a7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat národnosti', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
