<?php

declare(strict_types=1);

namespace Skadmin\Nationality\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Skadmin\Nationality\BaseControl;
use Skadmin\Nationality\Doctrine\Nationality\Nationality;
use Skadmin\Nationality\Doctrine\Nationality\NationalityFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;

class Overview extends GridControl
{
    use APackageControl;

    private NationalityFacade $facade;

    public function __construct(NationalityFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'nationality.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // DATA
        $translator = $this->translator;
        $dialYesNo  = Arrays::map(Constant::DIAL_YES_NO, static function ($text) use ($translator): string {
            return $translator->translate($text);
        });

        // COLUMNS
        $grid->addColumnText('name', 'grid.nationality.overview.name');
        $grid->addColumnText('shortName', 'grid.nationality.overview.short-name');
        $grid->addColumnText('alphaCode2', 'grid.nationality.overview.alpha-code2');
        $grid->addColumnText('alphaCode3', 'grid.nationality.overview.alpha-code3');
        $grid->addColumnText('isPreferred', 'grid.nationality.overview.is-preferred')
            ->setAlign('center')
            ->setReplacement($dialYesNo);

        // FILTER
        $grid->addFilterText('name', 'grid.nationality.overview.name', ['name', 'code']);
        $grid->addFilterText('shortName', 'grid.nationality.overview.short-name');
        $grid->addFilterText('alphaCode2', 'grid.nationality.overview.alpha-code2');
        $grid->addFilterText('alphaCode3', 'grid.nationality.overview.alpha-code3');
        $grid->addFilterSelect('isPreferred', 'grid.nationality.overview.is-preferred', $dialYesNo)
            ->setPrompt(Constant::PROMTP);

        $grid->setRowCallback(static function (Nationality $nationality, $tr): void {
            if (! $nationality->isPreferred()) {
                return;
            }

            $tr->addClass('font-weight-bold text-primary');
        });

        // OTHER
        $grid->setDefaultSort([
            'isPreferred' => 'DESC',
            'name'        => 'ASC',
        ]);

        return $grid;
    }
}
