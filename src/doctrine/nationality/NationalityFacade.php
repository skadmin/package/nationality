<?php

declare(strict_types=1);

namespace Skadmin\Nationality\Doctrine\Nationality;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

use function assert;
use function sprintf;

final class NationalityFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Nationality::class;
    }

    public function get(?int $id = null): Nationality
    {
        if ($id === null) {
            return new Nationality();
        }

        $nationality = parent::get($id);

        if ($nationality === null) {
            return new Nationality();
        }

        return $nationality;
    }

    /**
     * @return array<string, array<string>>
     */
    public function getPairsPreferred(string $namePreferred = 'preferred', string $nameUnpreferred = 'unpreferred'): array
    {
        $nationalitiesDb = $this->em
            ->getRepository($this->table)
            ->findAll();

        $nationalitiesPreferred   = [];
        $nationalitiesUnpreferred = [];

        foreach ($nationalitiesDb as $nationality) {
            assert($nationality instanceof Nationality);
            if ($nationality->isPreferred()) {
                $nationalitiesPreferred[$nationality->getId()] = sprintf('%s [%s]', $nationality->getName(), $nationality->getAlphaCode2());
            } else {
                $nationalitiesUnpreferred[$nationality->getId()] = sprintf('%s [%s]', $nationality->getName(), $nationality->getAlphaCode2());
            }
        }

        return [
            $namePreferred   => $nationalitiesPreferred,
            $nameUnpreferred => $nationalitiesUnpreferred,
        ];
    }
}
