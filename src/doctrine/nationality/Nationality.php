<?php

declare(strict_types=1);

namespace Skadmin\Nationality\Doctrine\Nationality;

use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
#[ORM\HasLifecycleCallbacks]
class Nationality
{
    use Entity\Id;
    use Entity\Name;

    #[ORM\Column(options: ['default' => false])]
    private bool $isPreferred = false;

    #[ORM\Column]
    private string $shortName = '';

    #[ORM\Column]
    private string $alphaCode2 = '';

    #[ORM\Column]
    private string $alphaCode3 = '';

    public function isPreferred(): bool
    {
        return $this->isPreferred;
    }

    public function getShortName(): string
    {
        return $this->shortName;
    }

    public function getAlphaCode2(): string
    {
        return $this->alphaCode2;
    }

    public function getAlphaCode3(): string
    {
        return $this->alphaCode3;
    }
}
